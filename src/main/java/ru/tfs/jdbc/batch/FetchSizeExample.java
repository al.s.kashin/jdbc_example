package ru.tfs.jdbc.batch;

import static ru.tfs.jdbc.Constants.DATABASE;
import static ru.tfs.jdbc.Constants.PASSWORD;
import static ru.tfs.jdbc.Constants.URL;
import static ru.tfs.jdbc.Constants.USER;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class FetchSizeExample {

    public static void main(String[] args) {
        String sql = "select id, external_id, internal_id from BATCH";

        try (
            Connection connection = DriverManager.getConnection(URL + DATABASE, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
        ) {
            //connection.setAutoCommit(false);
            LocalDateTime directBefore = LocalDateTime.now();
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getObject(1));
            }
            LocalDateTime directAfter = LocalDateTime.now();

            LocalDateTime batchBefore = LocalDateTime.now();
            statement.setFetchSize(100);
            ResultSet resultSet100 = statement.executeQuery();
            while (resultSet100.next()) {
                System.out.println(resultSet100.getObject(1));
            }
            LocalDateTime batchAfter = LocalDateTime.now();

            System.out.println();
            System.out.println();
            System.out.println();

            System.out.println("direct - " + ChronoUnit.MILLIS.between(directBefore, directAfter));
            System.out.println("batch - " + ChronoUnit.MILLIS.between(batchBefore, batchAfter));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
