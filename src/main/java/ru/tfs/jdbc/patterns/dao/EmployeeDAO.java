package ru.tfs.jdbc.patterns.dao;

import ru.tfs.jdbc.patterns.model.EmployeeVO;

public interface EmployeeDAO {
    EmployeeVO getById(Long id);
    EmployeeVO save(EmployeeVO employee);
    void delete(EmployeeVO employeeVO);
}
