package ru.tfs.jdbc.patterns.dao;

import static ru.tfs.jdbc.Constants.DATABASE;
import static ru.tfs.jdbc.Constants.PASSWORD;
import static ru.tfs.jdbc.Constants.URL;
import static ru.tfs.jdbc.Constants.USER;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ru.tfs.jdbc.patterns.OptimisticLockException;
import ru.tfs.jdbc.patterns.model.DepartmentVO;
import ru.tfs.jdbc.patterns.model.EmployeeVO;

public class PostgresEmployeeDAO implements EmployeeDAO {

    private static final String SELECT_QUERY = "select e.id, e.name, e.version, d.id, d.name, h.id, h.name "
        + "from p_employees e "
        + "join p_departments d on e.department_id = e.id "
        + "join p_employees h on h.id = d.head_id "
        + "where e.id = ? and e.delete_ts is null";

    private static final String UPDATE_QUERY = "update p_employees set "
        + "name = ?, department_id = ?, version = ? "
        + "where id = ? and version = ? and e.delete_ts is null";

    private static final String INSERT_QUERY = "insert into p_employees (id, name, department_id, version) "
        + "values (?, ?, ?, ?)";

    private static final String DELETE_QUERY = "update p_employees set delete_ts = ? where id = ?";

    @Override
    public EmployeeVO getById(Long id) {
        try (
            Connection connection = createConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_QUERY);
        ) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                return null;
            }

            EmployeeVO employee = new EmployeeVO();
            employee.setId(resultSet.getLong(1));
            employee.setName(resultSet.getString(2));

            DepartmentVO department = new DepartmentVO();
            department.setId(resultSet.getLong(3));
            department.setName(resultSet.getString(4));

            employee.setDepartment(department);

            long headId = resultSet.getLong(5);
            if (headId == 0L) {
                return employee;
            }

            if (employee.getId().equals(headId)) {
                department.setHead(employee);
                return employee;
            }

            EmployeeVO head = new EmployeeVO();
            head.setId(headId);
            head.setName(resultSet.getString(6));
            department.setHead(head);
            return employee;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public EmployeeVO save(EmployeeVO employee) {
        try (
            Connection connection = createConnection();
            PreparedStatement updateStatement = connection.prepareStatement(UPDATE_QUERY);
            PreparedStatement insertStatement = connection.prepareStatement(INSERT_QUERY);
        ) {
            if (employee.getId() != null) {
                updateStatement.setString(1, employee.getName());
                updateStatement.setLong(2, employee.getDepartment().getId());
                updateStatement.setLong(3, employee.getVersion() + 1);
                updateStatement.setLong(4, employee.getId());
                updateStatement.setInt(4, employee.getVersion());
                int count = updateStatement.executeUpdate();
                if (count == 0) {
                    throw new OptimisticLockException("Employee", employee.getId());
                }
            } else {
                PreparedStatement statement = connection.prepareStatement("select nextval('employees_seq')");
                ResultSet resultSet = statement.executeQuery();
                resultSet.next();
                Long id = resultSet.getLong(1);
                employee.setId(id);

                insertStatement.setLong(1, id);
                insertStatement.setString(2, employee.getName());
                insertStatement.setLong(3, employee.getDepartment().getId());
                insertStatement.setInt(4, 0);
                insertStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getById(employee.getId());
    }

    @Override
    public void delete(EmployeeVO employeeVO) {
        try (
            Connection connection = createConnection();
            PreparedStatement deleteStatement = connection.prepareStatement(DELETE_QUERY);
        ) {
            deleteStatement.setObject(1, LocalDateTime.now());
            deleteStatement.setLong(2, employeeVO.getId());
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL + DATABASE, USER, PASSWORD);
    }
}
