select * from employees e
    inner join departments d on e.department_id = d.id;

select * from employees e
    left join departments d on e.department_id = d.id;

select * from employees e
    right join departments d on e.department_id = d.id;

select * from employees e
    full join departments d on e.department_id = d.id;

select * from employees e
    cross join departments d;

select table_1.number, t.number from table_1
    join table_2 t on table_1.number = t.number;