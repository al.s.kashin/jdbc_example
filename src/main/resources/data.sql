INSERT INTO public.employees (id, name, department_id) VALUES (100, 'Иванов Иван', 100);
INSERT INTO public.employees (id, name, department_id) VALUES (101, 'Петров Петр', 101);
INSERT INTO public.employees (id, name, department_id) VALUES (102, 'Сидоров Сергей', 100);
INSERT INTO public.employees (id, name, department_id) VALUES (103, 'Семенов Семен', 101);

INSERT INTO public.departments (id, name, head_id) VALUES (101, 'Бухгалтерия', 101);
INSERT INTO public.departments (id, name, head_id) VALUES (100, 'Администрация', 100);