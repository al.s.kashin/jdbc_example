create table employees
(
	id bigint default nextval('employees_seq'::regclass) not null
		constraint employees_pk
			primary key,
	name varchar(512),
	department_id bigint not null
);

create index employees_department_index
	on employees (department_id);

create table departments
(
	id bigint default nextval('departments_seq'::regclass) not null
		constraint departments_pk
			primary key,
	name varchar(512),
	head_id bigint
		constraint departments_head_fk
			references employees
);

alter table employees
	add constraint employees_department_fk
		foreign key (department_id) references departments;

create index departments_head_index
	on departments (head_id);

